const possibleAnswers = [
    'It is certain.', 'It is decidedly so.', 'Without a doubt.', 'Yes – definitely.', 'You may rely on it.', 'As I see it, yes.', 'Most likely.', 'Outlook good.', 'Yes.', 'Signs point to yes.', 'Reply hazy, try again.', 'Ask again later.', 'Better not tell you now.', 'Cannot predict now.', 'Concentrate and ask again.', "Don't count on it.", 'My reply is no.', 'My sources say no.', 'Outlook not so good.', 'Very doubtful.'
]

const ballSubmit = document.getElementById('ballSubmit');
const answer = document.getElementById('answer');

function randomBall() {
    const randomAnswer = Math.floor(Math.random() * 20);
    let givenAnswer;

    for (let i = 1; i <= possibleAnswers.length; i++) {
        if (i === randomAnswer) {
            givenAnswer = possibleAnswers[i];
        }
    }

    answer.innerText = givenAnswer;
    answer.style.opacity = '1';

    let s = answer.style;
}

ballSubmit.addEventListener('click', randomBall);

